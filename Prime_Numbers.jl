using Plots
using Primes

plotly()
gr()
pyplot()

n = 100
A = zeros(Int, 0)
for i in 1:n
	push!(A, prime(i))
end

plot(x, A, seriestype = :scatter, title = "Prime values", lw = 1)

#x = 1:n
#xlabel!("n")
#ylabel!("prime value")
#plot!(1:n, 1:n)


Base.@kwdef mutable struct myPlot
    dt::Float64 = 0.02
    x::Float64 = 1
    y::Float64 = 1
end

function step!(l::myPlot, i)
    l.x += 1
	l.y += A[i]
end

attractor = myPlot()

# initialize a 3D plot with 1 empty series
plt = plot(1, 1 , title = "prime numbers", marker = 2,)

@gif for i=1:n
    step!(attractor, i)
    push!(plt, attractor.x, attractor.y)
end
