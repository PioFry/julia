using LinearAlgebra
using Random

function vararg(args...)
	return args
end

println(vararg(1, "string", 'c', true))

function defaults(a, b, x=5, y=6)
	return "$a $b and $x $y"
end
print(defaults((3, 222, 2)...))#rozpakowanie danych z krotki

function keyword_args(;kl=4, name2="hello")
	return Dict(:kl => kl, :name2 => name2)
end
print(keyword_args(name2 = :ness))

#lambdas
f3 = x -> x^2
f3(5)
f3("la ")
f3("ma")
(x -> x^3)(3)

#mapping
f(x) = x^4
print(map(f, [1, 2, 3]))#uses f for each element

A = rand(1:10, 3, 3)

print(A .+ 2 .* f3.(A) ./ A)

@. A .+ 2 .* f3.(A) ./ A #this is a makro

#assert
@assert f3(2) == 4
@assert f3(2) == 50

t(x) = (println(x); true)
print(t(1) && t(2))
f(x) = (println(x); false)
print(f(1) && t(2)) #short circuit evaluation 2nd func won't work
					#because when checking f1 operator && already got false
					#so it optimalizes process and doesn't check whats after &&

#zad1 napisz funkcje silnia

function silnia(x::BigInt)
	if x <= 1
		return false
	end
	temp = 1
	for i in 1:x
		temp *= i
	end
	return temp
end

print(@time silnia(big(20)))
print(@time silnia(20))

#zad2 wyznacz wartosc liczby pi przy pomocy metody monte carlo
#wskazówki: Koło wpisane w kwadrat, rown kola R^2=Y^2+X^2 tym sprawdzasz
#które punkty są w granicy koła, stosunek pól (a więc i punktów)
#koła do kwadratu powinno odpowiadać liczbie pi

function throwDarts(radius, num::Int) #radius of a square
	A = Array{Tuple{Float16, Float16}}(undef, 0)
	for i in 1:num
		# + rand(0:radius) cause rand(Float) has range [0:1)
		x = round(rand(Float16), digits = 1) + rand(0:radius)
		y = round(rand(Float16), digits = 1) + rand(0:radius)
		push!(A, tuple(x, y))
	end
	return A
end

function calculatePi(radius, darts)
	circleNum = 0
	for i in darts
		if((i[1] - radius)^2 + (i[2] - radius)^2 <= radius^2)
			circleNum += 1
		end
	end
	myPi = 4 * (circleNum / size(darts)[1])
	return myPi end

radius = 1
num = 1000000
print("\n\n")
arr = throwDarts(Float16(radius), num)
Base.print_matrix(stdout, arr)
print(calculatePi(radius, arr))
