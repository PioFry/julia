using Printf
using LinearAlgebra


print("Hello World\n")

n = 5
A = rand(1:10, n, n)
println(A)
r1 = A[1, :]
println(r1)
#n rows

for i in 1:n
    temp = A[i, :]
    println("row nr $i: $temp")
end

for i in 1:n
    temp = A[:, i]
    println("col nr $i: $temp")
end

d = Diagonal(A)
println(d)
Base.print_matrix(stdout, d)
print(" ")
Base.print_matrix(stdout, A)

trans = transpose(A)
print(" ")
Base.print_matrix(stdout, trans)

for i in 1:n
    rs = sum(A[:, i])
    println("row nr $i: $cs")
end

for i in 1:n
    cs = sum(A[i, :])
    println("col nr $i: $cs")
end
