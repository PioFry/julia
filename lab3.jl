using LinearAlgebra

function bisection(f, a)
	if f(a[1])*f(a[2]) > 0
		println("nie ma miejsca zerowego")
		return 0
	elseif f(a[1])*f(a[2]) == 0
		return f(a[1]) == 0 ? f(a[1]) : f(a[2])
	end
	c = 0
	while abs(a[1] - a[2]) > 1e-5
		c = (a[1] + a[2])/2
		if f(a[1])*f(c) < 0
			a[2] = c
		elseif f(c) == 0
			return c
		else
			a[1] = c
		end
		print("c val: $c\n")
	end
	return c
end

println(bisection(x->x^2-4., [0., 3.]))

#zad1 Napisz funkcję liczącą przybliżoną wartość funckji exp

function expo(x, e = 10^(-8))
	res = 0
	i = 0
	while ((x^i / factorial(big(i))) > e)
		res += x^i / factorial(big(i))
		i += 1
	end
	return res
end

#zad2 Napisz funkcję, która oblicza wartość pierwiastka
#kwadratowego z dowolnej liczby naturalnej  n  z precyzją  p .
#W tym celu wykorzystaj metodę https://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Bakhshali_method

function bakshali(s)
	pSq = 0
	N = 0
	sInt = trunc(Int, s)
	for i in sInt:-1:0
		for j in 1:i
			if(j * j == i)
				pSq = i
				N = j
				break
			end
		end
		if(pSq > 0)
			break
		end
	end
	d = s - pSq
	P = d / (2.0 * N)
	A = N + P
	sqrt_s = A - ((P * P) / (2.0 * A))
	return sqrt_s	end

print(bakshali(36))

#zad3 Zaimplementuj dowolną metodę iteracyjną znajdowania
#miejsc zerowych równania: Secant method

function Secant(x1::Int, x2::Int, e = 10^(-8))
	f(x) = x^2 - 8
	n=0
	xm=0
	x0=0
	c=0
	if(f(x1)*f(x2) < 0)
		while true
			x0 = ((x1 * f(x2) - x2 * f(x1)) / (f(x2) - f(x1)))
			c = f(x1) * f(x0)
			x1 = x2
			x2 = x0
			n += 1
			if(c == 0)
				break
			end
			xm = ((x1 * f(x2) - x2 * f(x1)) / (f(x2) - f(x1)))
			if(abs(xm - x0) < e)
				break
			end
		end
		#tmp = round(x0, 6)
		print("Root of the given equation = $x0\n")
		print("No. of iterations = $n\n")
	else
		print("Can not find a root in the given inteval\n")
	end end

Secant(0, 20)
